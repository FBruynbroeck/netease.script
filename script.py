# coding: utf-8
import requests
import os
import md5
import pipes

MAX_COUNT = 20
DOWNLOAD_DIR = "/Users/francois/Music/Songs/"
HEADERS = {
    "Cookie": "os=iPhone OS; appver=1.7.1;",
}


def search_song(keyword):
    r = requests.post(
        "http://music.163.com/api/search/get",
        data={
            "type": "1",
            "strategy": "1",
            "sub": False,
            "match": False,
            "limit": "20",
            "offset": "0",
            "s": keyword,
        },
        headers=HEADERS)
    return r.json().get('result')


def get_song(song_id):
    # GET http://music.163.com/api/song/detail?ids=[song_id]
    r = requests.get(
        'http://music.163.com/api/song/detail',
        params={
            'ids': '[%d]' % song_id
        },
        headers=HEADERS)

    result = r.json()
    return result['songs'][0]


def list_songs(songs, song_count):
    result = "=" * 20
    result += "\n"
    for i in xrange(song_count):
        song = songs[i]
        result += '[%d] %s - %s - %s\n' % (i+1, song['album']['name'].encode('utf8'), song['artists'][0]['name'].encode('utf8'), song['name'].encode('utf8'))
    result += "=" * 20
    result += "\n"
    return result


def encrypted_id(id):
    ''' from https://github.com/yanunon/NeteaseCloudMusic '''
    byte1 = bytearray('3go8&$8*3*3h0k(2)2')
    byte2 = bytearray(id)
    byte1_len = len(byte1)
    for i in xrange(len(byte2)):
        byte2[i] = byte2[i] ^ byte1[i % byte1_len]
    m = md5.new()
    m.update(byte2)
    result = m.digest().encode('base64')[:-1]
    result = result.replace('/', '_')
    result = result.replace('+', '-')
    return result


def download_song(music_id):
    os.system('cd %s; wget http://p1.music.126.net/%s/%s.mp3' % (DOWNLOAD_DIR, encrypted_id(music_id), music_id))


def main():
    """
    Search
    """
    keyword = raw_input('Search: ')
    search_result = search_song(keyword)
    songs = search_result.get('songs')
    if not songs:
        print 'No result'
        exit()
    song_count = int(search_result['songCount'])
    if song_count > MAX_COUNT:
        song_count = MAX_COUNT

    """
    Choice
    """
    print list_songs(songs, song_count)
    mid = raw_input('1-%s: ' % song_count)
    if not mid.isdigit() or int(mid) > song_count or int(mid) < 1:
        print 'It is not in the list'
        exit()

    """
    Get song
    """
    mid = int(mid) - 1
    song_id = songs[mid]['id']
    song = get_song(song_id)
    name = '%s - %s' % (song['artists'][0]['name'].encode('utf8'), song['name'].encode('utf8'))
    path = '%s/%s.mp3' % (DOWNLOAD_DIR, name)

    """
    Download
    """
    if os.path.isfile(path):
        print 'This song exist'
    else:
        # commit = raw_input('Download %s by %s? (\'no\' for exit)\n' % (song['name'].encode('utf8'), song['artists'][0]['name'].encode('utf8')))
        # if commit == 'no':
        #     exit()
        print 'Download %s by %s' % (song['name'].encode('utf8'), song['artists'][0]['name'].encode('utf8'))
        music = song['hMusic']
        if not music:
            music = song['mMusic']
            print 'Warning: Medium quality'
        music_id = str(music['dfsId'])
        download_song(music_id)
        actual_path = '%s/%s.mp3' % (DOWNLOAD_DIR, music_id)
        os.rename(actual_path, path)
    """
    Play
    """
    play = raw_input('Play this song ? (\'no\' for exist)')
    if play == 'no':
        exit()
    path = pipes.quote(path)
    os.system("open %s" % path)

if __name__ == '__main__':
    main()
