import glib, pygst
pygst.require("0.10")
import gst

class Player():
  def __init__(self):
    self.pipeline = gst.Pipeline("myPipeline")
    self.player = gst.element_factory_make("playbin", "theplayer")
    self.pipeline.add(self.player)
    self.audiosink = gst.element_factory_make("osxaudiosink", 'audiosink')
    self.audiosink.set_property('async-handling', True)
    self.player.set_property("uri", "http://http-live.sr.se/p1-mp3-192")
    self.pipeline.set_state(gst.STATE_PLAYING)

if __name__ == "__main__":
  Player()
  glib.MainLoop().run()
